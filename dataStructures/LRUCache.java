package dataStructures;

import java.util.HashMap;
import java.util.Map;

public class LRUCache<K, V> {
    private int capacity;
    private int size = 0;
    static class Entry<K, V>{
        K key;
        V value;
        Entry<K,V> prev = null;
        Entry<K,V> next = null;
        public Entry (K key, V value) {
            this.key = key;
            this.value = value;
        }
    }    
	Entry<K,V> head = null;
	Entry<K,V> tail = null;
	Map<K, Entry<K,V>> mymap;
	public LRUCache(int capacity) {
        this.capacity = capacity;
        mymap = new HashMap<K, Entry<K,V>>();
    }
    public void remove(Entry<K,V> entry) {
        Entry<K,V> prev = entry.prev;
        Entry<K,V> next = entry.next;
        entry.prev = null;
        entry.next = null;
        if (prev == null && next == null) { //entry is the only node
            head = null;
            tail = null;
        } else if (prev == null) {//entry is the head node
            next.prev = null;
            head = next;
        } else if (next == null) {//entry is the tail node
            prev.next = null;
            tail = prev;
        } else { //entry is the middle node
            prev.next = next;
            next.prev = prev;
        }
    }
    public void addToHead(Entry<K,V> entry){
        if (head == null){
            head = entry;
            tail = entry;
            return;
        }
        entry.next = head;
        head.prev = entry;
        head = entry;
    }
    public V get(K key) {
        if (!mymap.containsKey(key)) {
            return null;
        }
        Entry<K,V> entry = mymap.get(key);
        if (entry == head) {
            return entry.value;
        }
        remove(entry);
        V value = entry.value;
        addToHead(entry);
        return value;
    }
    public void set(K key, V value) {
        if (capacity == 0) {
            return;
        }
        if (get(key) == null) {
            if (size == capacity) { //delete tail
                mymap.remove(tail.key);
                remove(tail); 
            } else {
                size++;  
            }
            Entry<K,V> entry = new Entry<K,V>(key, value);
            mymap.put(key, entry);
            addToHead(entry);
            return;
        }
        //replace existing key's value
        Entry<K,V> entry = mymap.get(key);
        remove(entry);
        entry.value = value;
        mymap.put(key, entry);
        addToHead(entry);
    }
    public static void main(String[]args){
    	LRUCache<Integer,String> lru = new LRUCache<Integer,String>(4);
    	lru.set(1, "Hello");
    	lru.set(2, "World");
    	lru.set(3, "Java");
    	System.out.println(lru.get(1));
    	System.out.println(lru.get(2));
    	lru.set(1, "OODesign");
    	lru.set(4, "Algorithm");
    	lru.set(5, "Data");
    	System.out.println(lru.get(5));
    	lru.set(5, "Structure");
    	System.out.println(lru.get(6));
    	lru.set(6, "Greedy");
    	System.out.println(lru.get(6));
    	System.out.println(lru.get(2));
    	System.out.println(lru.get(1));
    	System.out.println(lru.get(4));
    	System.out.println(lru.get(3));
    }
}