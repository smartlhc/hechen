package dataStructures;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class NestedList {
	public List<Integer> flattenList(List<?> nestedList){
		List<Integer> out = new LinkedList<Integer>();
		flattenListHelper(nestedList, out);
		return out;		
	}
	public void flattenListHelper(List<?> nestedList, List<Integer> out) {
		for (Object nestedElem : nestedList) {
			if (nestedElem instanceof Integer ){
				out.add((int) nestedElem);				
			} else {
				flattenListHelper((List<?>) nestedElem, out);
			}
		}
	}
	//Sum the nested list. The value added to sum is the value of the number times the depth 
	//for example {1, {2, {3}}} will return 1 * 1 + 2 * 2 + 3 * 3 = 14
	//where depth incremented by one as the list goes to one level deeper
	public int sum(List<?> nestedList){
		return sumHelper(nestedList,  1);		
	}
	public int sumHelper(List<?> nestedList,  int depth){
		int result = 0;
		for (Object nestedElem: nestedList) {
			if (nestedElem instanceof Integer){ //base case: the value added to sum is number * the current depth
				result += (int) nestedElem * depth;				
			} else { // recursion call. Go one level deeper				
				result += sumHelper((List<?>)nestedElem, depth + 1);
			}
		}
		return result;
	}
	//Sum the nested list in reverse order. The value added is the number times the current depth
	//for example {1, {2, {3}}} will return 1 * 3 + 2 * 2 + 3 * 1 = 10
	//where depth is the maximum at the out most level;	
	public int reverseSum(List<?> nestedList) {
		//This variable is very important. It keeps updating max_depth so far
		//Use a single element array, which is equivalent to a reference in C++. 
		//So that you won't lose the depth after you return from the recursion call
		int []depth_so_far = {1};
		return reverseSumHelper(nestedList, depth_so_far);
	}
	public int reverseSumHelper(List<?> nestedList, int[] depth_so_far){
		int result = 0, sum_this_level = 0;
		for (Object nestedElem: nestedList) {
			if (nestedElem instanceof Integer){
				sum_this_level += (int) nestedElem;				
			} else {			
				result += reverseSumHelper((List<?>) nestedElem, depth_so_far);				
			}
		}	
		//Add the value to result at last, as the depth has been already updated
		result += sum_this_level * depth_so_far[0];
		depth_so_far[0]++;
		return result;
	}
	public static void main(String []args){
		List<Object> nestedList = a(1, a(2, a(3), 4));
		System.out.println("Before flattening");
		System.out.println(nestedList);
		NestedList nl = new NestedList();
		System.out.println("After flattening");
		System.out.println(nl.flattenList(nestedList));
		System.out.println("Sum the list: ");		
		System.out.println(nl.sum(nestedList));
		System.out.println("Sum the list in reverse order: ");	
		System.out.println(nl.reverseSum(nestedList));
	}
	private static List<Object> a(Object... a) {
		return Arrays.asList(a);
	}
}
