package dataStructures;

public class MyHashTable<K,V> {
	//initial capacity
	private int capacity = 4;
	private double threshold = 0.5; // a threshold that hash table will trigger size increase for hashtable
	private int size = 0;
	/*
	 * This class is only used inside MyHashTable and can be defined as inner class and static
	 * No other class outside will use it
	 */
	static class Entry<K,V> {
		K key;
		V value;
		Entry<K,V> next = null;
		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}
	}
	private Entry<K,V>[] array;
	@SuppressWarnings("unchecked")
	public MyHashTable() {
		array = new Entry[capacity];
	}
	public int hash(K key){
		//Use Math.abs can prevent overflow. Some machine fails to perform mod on negative integer
		return Math.abs(key.hashCode())% capacity;
	}
	public int size() {
		return this.size;
	}
	public void put(K key, V value){
		size += putHelper(key, value, array);
		double rate = (double) size/capacity;
		if (rate >= threshold) {
			resize();
		}				
	}
	//put the key value pair to the internal buffer, and return the size increased (0 or 1)
	public int putHelper(K key, V value, Entry<K,V> []buffer){
		int index = hash(key);
		if (buffer[index] == null) {
			buffer[index] = new Entry<K, V>(key, value);
			return 1;
		} else { // Collision detected. Use chaining
			Entry<K, V> curr = buffer[index];
			Entry<K, V> prev = null;
			while (curr != null) {
				if (curr.key.equals(key)){ //Has the same key. Replace, size does not change
					curr.value = value;
					return 0;
				}
				prev = curr;
				curr = curr.next;
			}			
			prev.next = new Entry<K, V>(key, value);
			return 1;			
		}		
	}
	//double the size
	public void resize(){
		capacity = capacity * 2;
		@SuppressWarnings("unchecked")
		Entry<K,V> []new_array = new Entry[capacity];				
		for (Entry<K,V> entry: array){
			while (entry != null){
				K key = entry.key;
				V value = entry.value;
				putHelper(key, value, new_array);
				entry = entry.next;
			}
		}
		array = new_array;
	}
	public boolean containsKey(K key){
		int index = hash(key);
		return array[index] != null;
	}
	public V get(K key){
		if (containsKey(key)){
			int index = hash(key);
			Entry<K,V> curr = array[index];
			while(curr != null) {
				if (curr.key.equals(key)){
					return curr.value;
				}
				curr = curr.next;
			}
		}
		return null;
	}	
	public V remove(K key) {
		if (containsKey(key)){
			int index = hash(key);
			Entry<K,V> curr = array[index];
			if (curr.key.equals(key)){ //the head node of the linked list
				Entry<K,V> ret = curr;
				curr = curr.next;
				array[index] = curr;
				ret.next = null;
				size--;
				return ret.value;
			}
			while (curr != null && curr.next != null){
				if (curr.next.key.equals(key)) {
					Entry<K,V> ret = curr.next;
					curr.next = curr.next.next;
					ret.next = null;
					size--;
					return ret.value;
				}
				curr = curr.next;
			}
		}
		return null;
	}
	public static void main(String []args){
		MyHashTable<Integer,String> ht = new MyHashTable<Integer, String>();
		ht.put(1, "Hello");
		ht.put(3, "Word");
		System.out.println("Get 1: " + ht.get(1));
		System.out.println("Get 3: " + ht.get(3));		
		System.out.println("Get 5: " + ht.get(5));
		ht.put(4, "MySQL");
		System.out.println("Get 4: " + ht.get(4));		
		ht.put(5, "Java");
		System.out.println("Get 5: " + ht.get(5));		
		ht.put(5, "Python");
		ht.put(17, "PHP");
		System.out.println("Current table size: " + ht.size());
		System.out.println("Get 5: " + ht.get(5));	
		System.out.println("Remove 5: " + ht.remove(5));
		System.out.println("Remove 5: " + ht.remove(5));
		System.out.println("Remove 1: " + ht.remove(1));
		System.out.println("Get 1: " + ht.get(1));
		System.out.println("Get 17: " + ht.get(17));	
		System.out.println("Current table size: " + ht.size());
	}
}
