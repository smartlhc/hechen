package searchingAndSorting;

import java.util.Arrays;

public class QuickSort {
	public void quickSort(int []arr){
		quickSort(arr, 0, arr.length - 1);
	}
	
	public void quickSort(int[] arr, int start, int end) {
		if (start >= end) {
			return;
		}
		int pos = partition(arr, start, end);
		quickSort(arr, start, pos - 1);
		quickSort(arr, pos + 1, end);
	}
	
	public int partition (int [] arr, int start, int end){
		int pivot = end;
		int pivot_val = arr[pivot];
		int left = start;
		for (int j = left; j <= end - 1; j++) {
			if (arr[j] <= pivot_val) {
				swap (arr, left, j);
				left++;
			} 
		}
		swap (arr, left, end);
		return left;
	}
	
	public void swap(int []arr, int i, int j){
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static void main(String []args){
		int [] arr = {2, 1, 4, 3, 6, 5, 9, 8};
		System.out.println("Before sorting: ");		
		System.out.println(Arrays.toString(arr));
		QuickSort qs = new QuickSort();
		qs.quickSort(arr);
		System.out.println("After sorting using quicksort: ");
		System.out.println(Arrays.toString(arr));	
	}
}
