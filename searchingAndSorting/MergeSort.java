package searchingAndSorting;

import java.util.Arrays;

public class MergeSort {
	public void mergeSort(int []arr) {
		if (arr.length <= 1) {
			return;
		}
		int len = arr.length;
		int [] left = new int[len / 2];
		System.arraycopy(arr, 0, left, 0, len / 2);
		mergeSort(left);
		int [] right = new int[len - len / 2];
		System.arraycopy(arr, len / 2, right, 0, len - len / 2);		
		mergeSort(right);
		int [] temp = merge(left, right);
		System.arraycopy(temp, 0, arr, 0, temp.length);		
	}
	public int [] merge(int[] left, int[] right) {
		int i = 0, j = 0;
		int [] temp = new int[left.length + right.length];
		int index = 0;
		while (i < left.length && j  < right.length) {
			if (left[i] < right[j]) {
				temp[index] = left[i];
				i++;
			} else {
				temp[index] = right[j];
				j++;
			}
			index++;
		}
		while (i < left.length) {
			temp[index++] = left[i++];
		}
		while (j < right.length){
			temp[index++] = right[j++];
		}
		return temp;		
	}
	public static void main(String [] args) {
		int [] arr = {2, 1, 4, 3, 6, 5, 9, 8};
		System.out.println("Before sorting: ");
		System.out.println(Arrays.toString(arr));		
		MergeSort ms = new MergeSort();
		ms.mergeSort(arr);
		System.out.println("After sorting using merge sort: ");
		System.out.println(Arrays.toString(arr));		
	}
}
