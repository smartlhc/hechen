package searchingAndSorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
//Define a wrapper class, which contains the from variable, 
//To keep track of where the element in the heap is from
class Wrapper {
	int value;
	int from;
	int index;
	public Wrapper(int value, int from, int index) {
		this.value = value;
		this.from = from;
		this.index = index;
	}
}
public class MergeKSortedArrays {
	//Define a comparator, to compare the wrapper objects, 
	//which will be used as the comparator for the priority queue
	private Comparator<Wrapper> cmp = new Comparator<Wrapper>(){
		@Override
		public int compare(Wrapper w1, Wrapper w2) {
			return w1.value - w2.value;
		}
	};
	public int[] mergeKArrays(int [][]arrays){
		int k = arrays.length;
		if (k == 0) {
			return new int[0];
		}
		int n = arrays[0].length;
		if (n == 0) {
			return new int[0];
		}
		int [] result = new int[n * k];
		//Use the priority queue to peek the smallest one at each step
		PriorityQueue<Wrapper> heap = new PriorityQueue<Wrapper>(cmp);
		for (int i = 0; i < k; i++) {
			Wrapper w = new Wrapper(arrays[i][0], i, 0);
			heap.offer(w);
		}
		int j = 0;
		//This index[] array maintain the current indexes status of all k arrays
		//If the ith array as the smallest element so far, array[i][index[i]]. 
		//We picked it from the queue, and we will add its next element back to the queue, 
		//which is array[i][index[i]+1]
		
		while(!heap.isEmpty()){
			Wrapper top = heap.poll();
			result[j] = top.value;
			j++;
			int from = top.from;
			int index = top.index;
			if (index < n - 1) {
				Wrapper next = new Wrapper(arrays[from][index + 1], from, index + 1);
				heap.offer(next);
			}
		}
		return result;
	}
	public static void main(String [] args){
		int [][] arrays = {{1, 4, 8, 14}, {2, 5, 7, 16}, {3, 6, 10, 12}, {9, 11, 13, 15}};
		MergeKSortedArrays mks = new MergeKSortedArrays();
		int [] result = mks.mergeKArrays(arrays);
		System.out.println("After merge: " + Arrays.toString(result));
	}
}
