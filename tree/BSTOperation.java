package tree;
class TreeNode{
	int val;
	TreeNode left;
	TreeNode right;
	public TreeNode (int val){
		this.val = val;
	}
}
public class BSTOperation {
	public TreeNode search(TreeNode root, int key){
		while(root != null){
			if(key == root.val){
				return root;
			}
			if (key < root.val){
				root = root.left;
			} else {
				root = root.right;
			}
		}
		return null;		
	}
	public TreeNode insert(TreeNode root, int key){
		TreeNode parent = null;
		TreeNode curr = root;
		while (curr != null){
			parent = curr;
			if (key < curr.val){
				curr = curr.left;
			} else {
				curr = curr.right;
			}
		}
		TreeNode node = new TreeNode(key);
		if (parent == null){
			root = node;
			return root;
		}
		if (key < parent.val){
			parent.left = node;
		} else {
			parent.right = node;
		}
		return root;
	}
	public TreeNode delete(TreeNode root, int key){
		TreeNode parent = null;
		TreeNode curr = root;
		while (curr != null && curr.val != key){
			parent = curr;
			if (key < curr.val){
				curr = curr.left;
			} else {
				curr = curr.right;
			} 							
		}
		if (parent == null || curr == null){
			return null;
		}
		//find key, delete
		if (curr.left == null || curr.right == null){
			TreeNode child = curr.left == null ? curr.right: curr.left;
			if (curr == parent.left){
				parent.left = child;
			} else {
				parent.right = child;
			}			
		} else {
			//find the in order successor of curr
			TreeNode succ = curr.right;
			TreeNode p = curr;
			while (succ != null && succ.left != null){
				p = succ;
				succ = succ.left;
			}
			curr.val = succ.val;
			if (succ == p.left){
				p.left = succ.right;
			} else {
				p.right = succ.right;
			}
		}
		return root;
	}
}
